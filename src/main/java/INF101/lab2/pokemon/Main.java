package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1 = new Pokemon ("Chessie", 150, 90);
    public static Pokemon pokemon2 = new Pokemon ("BlastDisaster",150, 20);
    public static void main(String[] args) {
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated
        System.out.println(pokemon1);
        System.out.println(pokemon2);
        System.out.println("");

        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            pokemon1.attack(pokemon2);       
            if (!pokemon2.isAlive()){
                break;
            }
        
    
            pokemon2.attack(pokemon1);
            if (!pokemon1.isAlive()){
                break;
                }
       }
   }  
}



